package softagi.ss.firstapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        getData();
    }

    private void getData()
    {
        dataModel model = (dataModel) getIntent().getSerializableExtra("data");
        TextView textView = findViewById(R.id.result_text);
        textView.setText(model.getName() + "\n" + model.getTotal() + "\n" + model.getCategory());
    }
}