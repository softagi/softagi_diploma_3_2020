package softagi.ss.firstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    private EditText nameField;
    private RadioGroup radioGroup;
    private CheckBox chocolateCb,moccaCb,creamCb;
    private TextView cupsText;

    // 7 for chocolate
    // 5 for mocca
    // 3 for cream
    private int adds = 0;

    // 15 for espresso
    // 10 for french
    // 7 for turkish
    private int price = 0;
    private String cat;
    private int cupsCount = 1;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      initViews();
    }

    private void initViews()
    {
        nameField = findViewById(R.id.name_field);
        radioGroup = findViewById(R.id.radio_gp);
        chocolateCb = findViewById(R.id.chocolate_ckb);
        moccaCb = findViewById(R.id.mocca_ckb);
        creamCb = findViewById(R.id.cream_ckb);
        cupsText = findViewById(R.id.cups_text);
    }

    public void button(View view)
    {
        switch (view.getId())
        {
            case R.id.add_btn:
                if (cupsCount == 10)
                {
                    Toast.makeText(this, "can not order more than 10 cups at once", Toast.LENGTH_SHORT).show();
                } else
                    {
                        cupsCount += 1;
                        cupsText.setText(String.valueOf(cupsCount));
                    }
                break;
            case R.id.minus_btn:
                if (cupsCount == 1)
                {
                    Toast.makeText(this, "can not order less than 1 cup at once", Toast.LENGTH_SHORT).show();
                } else
                    {
                        cupsCount -= 1;
                        cupsText.setText(String.valueOf(cupsCount));
                    }
                break;
            case R.id.order_now_btn:
                orderNow();
                break;
        }
    }

    private void orderNow()
    {
        name = nameField.getText().toString();

        if (name.isEmpty())
        {
            Toast.makeText(this, "please enter your name", Toast.LENGTH_SHORT).show();
            return;
        }

        if (price == 0)
        {
            Toast.makeText(this, "please choose coffee", Toast.LENGTH_SHORT).show();
            return;
        }

        if (chocolateCb.isChecked())
        {
            adds = adds + 7;
        }

        if (moccaCb.isChecked())
        {
            adds = adds + 5;
        }

        if (creamCb.isChecked())
        {
            adds = adds + 3;
        }

        int total = (price + adds) * cupsCount;

        dataModel model = new dataModel(name, total, cat);

        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("data", model);
        startActivity(intent);
    }

    public void radioButtons(View view)
    {
        // Is the button now checked?
        RadioButton radioButton = (RadioButton) view;
        boolean checked = radioButton.isChecked();

        // Check which radio button was clicked
        switch(view.getId())
        {
            case R.id.espresso_rb:
                if (checked)
                {
                    price = 15;
                    cat = "espresso";
                    //Toast.makeText(this, String.valueOf(price), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.french_rb:
                if (checked)
                {
                    price = 10;
                    cat = "french";
                    //Toast.makeText(this, String.valueOf(price), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.turkish_rb:
                if (checked)
                {
                    price = 7;
                    cat = "turkish";
                    //Toast.makeText(this, String.valueOf(price), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}