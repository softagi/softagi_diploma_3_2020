package softagi.ss.firstapp;

import java.io.Serializable;

public class dataModel implements Serializable
{
    private String name;
    private int total;
    private String category;

    public dataModel(String name, int total, String category) {
        this.name = name;
        this.total = total;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}